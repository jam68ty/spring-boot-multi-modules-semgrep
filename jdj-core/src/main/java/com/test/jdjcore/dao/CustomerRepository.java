package com.test.jdjcore.dao;

import com.test.jdjcore.entity.Customer;

public interface CustomerRepository {
    Customer getCustomerById(int id);
}
