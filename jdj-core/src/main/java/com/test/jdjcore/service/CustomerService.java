package com.test.jdjcore.service;

import com.test.jdjcore.dao.CustomerRepository;
import com.test.jdjcore.entity.Customer;
import org.springframework.stereotype.Service;

@Service("customerService")
public class CustomerService implements CustomerRepository{


    @Override
    public Customer getCustomerById(int id) {
        return new Customer(id, "name", "email");
    }
}
