package com.test.jdjweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.test.jdjcore", "com.test.jdjweb"})
public class JdjWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(JdjWebApplication.class, args);
    }

}
